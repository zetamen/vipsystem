/* VIPSystemItems v1.5 */

#include <amxmodx>
#include <VIPSystem>

new Array:vipItemNameArray;
new Array:vipItemFlagArray;
new Array:vipItemStateArray;

new forwardItemSelected, forwardOpeningMenu;

new itemsNumber;

public plugin_init() 
{
	register_plugin("VIPSystemItems", "1.5", "ZETA [M|E|N]");
	
	register_dictionary("VIPSystem.txt");
	
	forwardItemSelected = CreateMultiForward("VSItemSelected", ET_CONTINUE, FP_CELL, FP_CELL);
	forwardOpeningMenu = CreateMultiForward("VSOpeningMenu", ET_CONTINUE, FP_CELL);
	
	itemsNumber = 0;
	
	register_clcmd("vip_menu", "ShowVipMenuPre", ADMIN_ALL, "Show Vip Menu");
}

public plugin_precache()
{
	vipItemNameArray = ArrayCreate(81, 1);
	vipItemFlagArray = ArrayCreate(1, 1);
	vipItemStateArray  = ArrayCreate(1, 1);
}

// Natives

public plugin_natives()
{
	register_native("VSRegisterItem", "NativeRegisterItem", 1);
	register_native("VSGetItemState", "NativeGetItemState", 1);
	register_native("VSEnableItem", "NativeEnableItem", 1);
	register_native("VSDisableItem", "NativeDisableItem", 1);
	register_native("VSSetItemName", "NativeSetItemName", 1);
}

public NativeRegisterItem(name[], flag)
{
	param_convert(1);
	
	ArrayPushString(vipItemNameArray, name);
	ArrayPushCell(vipItemFlagArray, flag);
	ArrayPushCell(vipItemStateArray, 1);
	itemsNumber++;
	
	return itemsNumber - 1;
}

public NativeEnableItem(id)
{
	ArraySetCell(vipItemStateArray, id, 1);
}

public NativeDisableItem(id)
{
	ArraySetCell(vipItemStateArray, id, 0);
}

public NativeGetItemState(id)
{
	return ArrayGetCell(vipItemStateArray, id);
}

public NativeSetItemName(id, name[])
{
	param_convert(2);
	
	ArraySetString(vipItemNameArray, id, name);
}

// Menu

public ShowVipMenuPre(id)
{
	if (!VSGetUserVip(id))
	{
		client_print(id, print_console, "%L", id, "NO_ACC_COM");
		return PLUGIN_HANDLED;
	}
	
	if (!itemsNumber)
	{
		client_print(id, print_console, "%L", id, "NO_ITEMS");
		return PLUGIN_HANDLED;
	}
	
	new result;
	ExecuteForward(forwardOpeningMenu, result, id);
	
	if (result == PLUGIN_HANDLED)
	{
		return PLUGIN_HANDLED;
	}
	
	ShowVipMenu(id, 0);
	return PLUGIN_HANDLED;
}

public ShowVipMenu(id, page)
{	
	new temp[36];
	format(temp, charsmax(temp), "%L", id, "VIP_MENU");
	
	new menu = menu_create(temp, "VipMenuHandler");
	
	new item[32], itemName[32], num[3], flag, itemState;
	for (new i = 0; i < itemsNumber; i++)
	{
		flag = ArrayGetCell(vipItemFlagArray, i);
		
		if (VSGetVipFlag(id, flag))
		{
			itemState = ArrayGetCell(vipItemStateArray, i);
			ArrayGetString(vipItemNameArray, i, item, charsmax(item));

			format(itemName, charsmax(itemName), "%s%s", (itemState ? "\w" : "\d"), item);
			num_to_str(i, num, charsmax(num));
			menu_additem(menu, itemName, num);
		}
	}
	
	format(temp, charsmax(temp), "%L", id, "MENU_NEXT");
	menu_setprop(menu, MPROP_NEXTNAME, temp);
	format(temp, charsmax(temp), "%L", id, "MENU_BACK");
	menu_setprop(menu, MPROP_BACKNAME, temp);
	format(temp, charsmax(temp), "%L", id, "MENU_EXIT");
	menu_setprop(menu, MPROP_EXITNAME, temp);
	
	menu_display(id, menu, page);
	return PLUGIN_HANDLED;
}

public VipMenuHandler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	
	new buffer[3], dummy;
	menu_item_getinfo(menu, item, dummy, buffer, charsmax(buffer), _, _, dummy);
	
	new item = str_to_num(buffer);
	new itemState = ArrayGetCell(vipItemStateArray, item);
	
	if (itemState)
	{
		new result;
		ExecuteForward(forwardItemSelected, result, id, item);
	}
	else
	{
		new page = floatround(float(item) / 7, floatround_floor);
		ShowVipMenu(id, page);
	}
	
	return PLUGIN_HANDLED;
}

public plugin_end()
{
	ArrayDestroy(vipItemNameArray);
	ArrayDestroy(vipItemFlagArray);
	ArrayDestroy(vipItemStateArray);
}
