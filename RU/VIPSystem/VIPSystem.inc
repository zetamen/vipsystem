/* VIPSystem API v1.5 */

/* Флаги доступа */
#define VIP_FLAG_ALL 0      // Любой флаг
#define VIP_FLAG_A (1<<0)   // Флаг "a"
#define VIP_FLAG_B (1<<1)   // Флаг "b"
#define VIP_FLAG_C (1<<2)   // Флаг "c"
#define VIP_FLAG_D (1<<3)   // Флаг "d"
#define VIP_FLAG_E (1<<4)   // Флаг "e"
#define VIP_FLAG_F (1<<5)   // Флаг "f"
#define VIP_FLAG_G (1<<6)   // Флаг "g"
#define VIP_FLAG_H (1<<7)   // Флаг "h"
#define VIP_FLAG_I (1<<8)   // Флаг "i"
#define VIP_FLAG_J (1<<9)   // Флаг "j"
#define VIP_FLAG_K (1<<10)  // Флаг "k"
#define VIP_FLAG_L (1<<11)  // Флаг "l"
#define VIP_FLAG_M (1<<12)  // Флаг "m"
#define VIP_FLAG_N (1<<13)  // Флаг "n"
#define VIP_FLAG_O (1<<14)  // Флаг "o"
#define VIP_FLAG_P (1<<15)  // Флаг "p"
#define VIP_FLAG_Q (1<<16)  // Флаг "q"
#define VIP_FLAG_R (1<<17)  // Флаг "r"
#define VIP_FLAG_S (1<<18)  // Флаг "s"
#define VIP_FLAG_T (1<<19)  // Флаг "t"
#define VIP_FLAG_U (1<<20)  // Флаг "u"
#define VIP_FLAG_V (1<<21)  // Флаг "v"
#define VIP_FLAG_W (1<<22)  // Флаг "w"
#define VIP_FLAG_X (1<<23)  // Флаг "x"
#define VIP_FLAG_Y (1<<24)  // Флаг "y"
#define VIP_FLAG_Z (1<<25)  // Флаг "z"

/* Возвращает 1, если игрок вип, в противном случае 0 */
native VSGetUserVip(id)

/* Возвращает 1, если игрок имеет указанный флаг, в противном случае 0 */
native VSGetVipFlag(id, flag)

/* Возвращает флаги игрока, в виде битовой последовательности */
native VSGetVipFlags(id)

/* Регистрирует новую возможность
Параметры: 
	name - название возможности
	flag - флаг доступа
Возвращает:
	id возможности
*/
native VSRegisterItem(name[], flag)

/* Возвращает 1, если привилегия активна, в противном случае 0 */
native VSGetItemState(id)

/* Активирует привилегию в меню */
native VSEnableItem(id)

/* Отключает привилегию в меню */
native VSDisableItem(id)

/* Устанавливает название привилегии в меню */
native VSSetItemName(id, name[])

/* Добавляет флаги игроку */
native VSAddVipFlags(id, flags)

/* Событие выбора возможности */
forward VSItemSelected(id, itemid)

/* Событие подключения випа */
forward VSVipConnect(id)

/* Событие открытия меню */
forward VSOpeningMenu(id)

/* Преобразует строку во флаги, в виде битовой последовательности */
stock VSStrToFlags(const str[])
{
	new bin = 0;
	new len = strlen(str);
	
	for (new i = 0; i < len; ++i)
	{
		bin |= (1<<(str[i] - 'a'));
	}
	
	return bin;
}

/* Преобразует флаги в строку */
stock VSFlagsToStr(const bits, str[])
{
	new ch[2];
	
	for (new i = 0, len = 0; i < 27; ++i)
	{
		if (bits & (1<<i))
		{
			format(ch, charsmax(ch), "%c", (i + 'a'));
			strcat(str, ch, ++len);
		}
	}
}
