/* VSDeathrunPack v1.5

Описание:
	Набор плагинов для Deathrun
Плагины:
	VSAdditionalLivesItem
	VSBecomeTerroristItem
	VSGravityItem
	VSIncreaseScoreItem
	VSInvisItem
	VSLongJumpItem
	VSVipSpeedItem
*/

#include <amxmodx>
#include <VIPSystem>
#include <fakemeta_util>
#include <fun>
#include <cstrike>
#include <hamsandwich>

#define INVIS_ACCESS_FLAG VIP_FLAG_A
#define SPEED_ACCESS_FLAG VIP_FLAG_B
#define GRAVITY_ACCESS_FLAG VIP_FLAG_C
#define LONGJUMP_ACCESS_FLAG VIP_FLAG_D
#define ADDITIONAL_LIVES_ACCESS_FLAG VIP_FLAG_E
#define BECOME_TERRORIST_ACCESS_FLAG VIP_FLAG_F
#define INCREASE_SCORE_ACCESS_FLAG VIP_FLAG_G

new cvarVipSpeed, cvarLongjumpForce, cvarLongjumpHeight, cvarLongjumpCooldown, 
cvarAmountScore, cvarGravity, cvarAdditionalLivesAmount;

new additionalLives[33];
new bool:speed[33];
new bool:longJump[33];
new Float:lastLongJumpTime[33];
new bool:invis[33];
new maxPlayers;
new msgidScoreInfo;

new speedItem, longJumpItem, invisItem, increaseScoreItem, gravityItem, 
becomeTerroristItem, additionalLivesItem;

public plugin_init()
{
	register_plugin("VSDeathrunPack", "1.5", "ZETA [M|E|N]");
	
	cvarVipSpeed = register_cvar("vs_vip_speed", "290");
	cvarLongjumpForce = register_cvar("vs_longjump_force", "550");
	cvarLongjumpHeight = register_cvar("vs_longjump_height", "255");
	cvarLongjumpCooldown = register_cvar("vs_longjump_cooldown", "5.0");
	cvarAmountScore = register_cvar("vs_amount_score", "2");
	cvarGravity = register_cvar("vs_gravity", "0.6");
	cvarAdditionalLivesAmount = register_cvar("vs_additional_lives_amount", "2");
	
	speedItem = VSRegisterItem("Speed", SPEED_ACCESS_FLAG);
	longJumpItem = VSRegisterItem("Long Jump", LONGJUMP_ACCESS_FLAG);
	invisItem = VSRegisterItem("Invisibility", INVIS_ACCESS_FLAG);
	increaseScoreItem = VSRegisterItem("Increase Score", INCREASE_SCORE_ACCESS_FLAG);
	gravityItem = VSRegisterItem("Gravity", GRAVITY_ACCESS_FLAG);
	becomeTerroristItem = VSRegisterItem("Become Terrorist", BECOME_TERRORIST_ACCESS_FLAG);
	additionalLivesItem = VSRegisterItem("Additional Lives", ADDITIONAL_LIVES_ACCESS_FLAG);
	
	register_event("HLTV", "EventRoundStart", "a", "1=0", "2=0");
	register_event("CurWeapon", "EventCurWeapon", "be", "1=1");
	register_event("DeathMsg", "EventDeath", "a");
	
	register_forward(FM_PlayerPreThink, "EventPlayerPreThink");
	
	maxPlayers = get_maxplayers();
	msgidScoreInfo = get_user_msgid("ScoreInfo");
}

public VSItemSelected(id, itemid)
{
	if (itemid == speedItem)
	{
		speed[id] = true;
	}
	else if (itemid == longJumpItem)
	{
		longJump[id] = true;
	}
	else if (itemid == invisItem)
	{
		fm_set_user_rendering(id, kRenderFxGlowShell, 0, 0, 0, kRenderTransAlpha, 20);
		invis[id] = true;
	}
	else if (itemid == increaseScoreItem)
	{
		SetUserFrags(id, get_user_frags(id) + get_pcvar_num(cvarAmountScore));
	}
	else if (itemid == gravityItem)
	{
		set_user_gravity(id, get_pcvar_float(cvarGravity));
	}
	else if (itemid == becomeTerroristItem)
	{
		user_silentkill(id);
		cs_set_user_team(id, CS_TEAM_T);
		ExecuteHamB(Ham_CS_RoundRespawn, id);
	}
	else if (itemid == additionalLivesItem)
	{
		additionalLives[id] = get_pcvar_num(cvarAdditionalLivesAmount);
	}
}

public client_disconnect(id)
{
	speed[id] = false;
	longJump[id] = false;
	additionalLives[id] = 0;
	invis[id] = false;
}

public EventRoundStart()
{
	arrayset(longJump, false, 33);
	arrayset(speed, false, 33);
	arrayset(additionalLives, 0, 33);
	
	for (new id = 1; id < maxPlayers; ++id)
	{
		if (invis[id])
		{
			fm_set_user_rendering(id);
			invis[id] = false;
		}
	}
}

public EventDeath()
{
	new victim = read_data(2);
	
	if (additionalLives[victim])
	{
		set_task(0.5, "spawnPlayer", victim);
		
		--additionalLives[victim];
	}
}

public spawnPlayer(id)
{
	ExecuteHamB(Ham_CS_RoundRespawn, id);
}
	
public EventPlayerPreThink(id)
{
	if (!is_user_alive(id))
	{
		return FMRES_IGNORED;
	}
	
	if (AllowLongJump(id))
	{
		static Float:velocity[3];
		velocity_by_aim(id, get_pcvar_num(cvarLongjumpForce), velocity);
		
		velocity[2] = get_pcvar_float(cvarLongjumpHeight);
		
		set_pev(id, pev_velocity, velocity);
		
		lastLongJumpTime[id] = get_gametime();
	}
	
	return FMRES_IGNORED;
}

AllowLongJump(id)
{
	static buttons;
	buttons = pev(id, pev_button);
	
	if (!(buttons & IN_JUMP) || !(buttons & IN_DUCK))
		return false;
	
	if (!longJump[id])
		return false;
	
	if (!(pev(id, pev_flags) & FL_ONGROUND) || fm_get_speed(id) < 80)
		return false;

	if (get_gametime() - lastLongJumpTime[id] < get_pcvar_float(cvarLongjumpCooldown))
		return false;
	
	return true;
}

public EventCurWeapon(id)
{
	if (speed[id])
	{
		fm_set_user_maxspeed(id, get_pcvar_float(cvarVipSpeed));
	}
}

stock SetUserFrags(id, frags)
{
	set_pev(id, pev_frags, float(frags));
	
	new deaths = get_user_deaths(id);
	new team = get_user_team(id);
		
	message_begin(MSG_ALL, msgidScoreInfo);
	write_byte(id);
	write_short(frags);
	write_short(deaths);
	write_short(0);
	write_short(team);
	message_end();
}