/* VSVipsOnline v1.3

��������:
	���������� ����� � ����.
�������:
	say /vips
*/

#include <amxmodx>
#include <VIPSystem>

new maxPlayers;

public plugin_init() 
{
	register_plugin("VSVipsOnline", "1.3", "ZETA [M|E|N]");
	
	maxPlayers = get_maxplayers();
	
	register_clcmd("say /vips", "ShowVipsOnline", ADMIN_ALL, "Show Vips Online");
}

public ShowVipsOnline(id)
{
	new message[256], name[32], count = 0;
	new len = format(message, charsmax(message), "Vips Online: ");
	
	for (new player = 1; player <= maxPlayers; ++player)
	{
		if (is_user_connected(player) && VSGetUserVip(player))
		{
			if (len > 96) 
			{
				client_print(id, print_chat, "%s,", message);
				len = format(message, charsmax(message), "");
			}
			
			get_user_name(player, name, charsmax(name));
			
			if (count && len)
			{
				len += format(message[len], 255 - len, ", ");
			}
			
			len += format(message[len], 255 - len, "%s", name);
			
			++count;
		}
	}
	
	if (len)
	{
		if (!count)
		{
			len += format(message[len], 255 - len, "No vips online.");
		}
		
		client_print(id, print_chat, "%s", message);
	}
	
	return PLUGIN_HANDLED;
}
