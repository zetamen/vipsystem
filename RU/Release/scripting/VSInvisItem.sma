/* VSInvisItem v1.3

��������:
	���� �����������.
���� ������� �� ���������:
	VIP_FLAG_A.
��� �����������:
	����������.
*/

#include <amxmodx>
#include <VIPSystem>
#include <fakemeta_util>

#define ACCESS_FLAG VIP_FLAG_A

new bool:invis[33];
new maxPlayers;

new curItem;

public plugin_init() 
{
	register_plugin("VSInvisItem", "1.3", "ZETA [M|E|N]");
	curItem = VSRegisterItem("Invisibility", ACCESS_FLAG);
	
	register_event("HLTV", "EventNewRound", "a", "1=0", "2=0");
	
	maxPlayers = get_maxplayers();
}

public VSItemSelected(id, itemid)
{
	if (itemid == curItem)
	{
		fm_set_user_rendering(id, kRenderFxGlowShell, 0, 0, 0, kRenderTransAlpha, 20);
		invis[id] = true;
	}
}

public EventNewRound()
{
	for (new id = 1; id < maxPlayers; ++id)
	{
		if (invis[id])
		{
			fm_set_user_rendering(id);
			invis[id] = false;
		}
	}
}