/* VSVipPrefixColoredTranslit v1.3 

��������:
	��������� ������� [VIP] � ����. ������������� � ColoredTranslit.
���� ������� �� ���������:
	VIP_FLAG_ALL.
��� �����������:
	����������.
*/

#include <amxmodx>
#include <VIPSystem>
#include <colored_translit>

#define ACCESS_FLAG VIP_FLAG_ALL

public plugin_init() {
	register_plugin("VSVipPrefixColoredTranslit", "1.3", "ZETA [M|E|N]");
}

public ct_message_format(id)
{
	if(VSGetVipFlag(id, ACCESS_FLAG))
	{
		ct_add_to_msg(CT_MSGPOS_PREFIX, "[^x04VIP^x01]");
	}
	
	return PLUGIN_CONTINUE;
} 
