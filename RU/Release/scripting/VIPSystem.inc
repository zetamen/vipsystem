/* VIPSystem API v1.5 */

/* ����� ������� */
#define VIP_FLAG_ALL 0      // ����� ����
#define VIP_FLAG_A (1<<0)   // ���� "a"
#define VIP_FLAG_B (1<<1)   // ���� "b"
#define VIP_FLAG_C (1<<2)   // ���� "c"
#define VIP_FLAG_D (1<<3)   // ���� "d"
#define VIP_FLAG_E (1<<4)   // ���� "e"
#define VIP_FLAG_F (1<<5)   // ���� "f"
#define VIP_FLAG_G (1<<6)   // ���� "g"
#define VIP_FLAG_H (1<<7)   // ���� "h"
#define VIP_FLAG_I (1<<8)   // ���� "i"
#define VIP_FLAG_J (1<<9)   // ���� "j"
#define VIP_FLAG_K (1<<10)  // ���� "k"
#define VIP_FLAG_L (1<<11)  // ���� "l"
#define VIP_FLAG_M (1<<12)  // ���� "m"
#define VIP_FLAG_N (1<<13)  // ���� "n"
#define VIP_FLAG_O (1<<14)  // ���� "o"
#define VIP_FLAG_P (1<<15)  // ���� "p"
#define VIP_FLAG_Q (1<<16)  // ���� "q"
#define VIP_FLAG_R (1<<17)  // ���� "r"
#define VIP_FLAG_S (1<<18)  // ���� "s"
#define VIP_FLAG_T (1<<19)  // ���� "t"
#define VIP_FLAG_U (1<<20)  // ���� "u"
#define VIP_FLAG_V (1<<21)  // ���� "v"
#define VIP_FLAG_W (1<<22)  // ���� "w"
#define VIP_FLAG_X (1<<23)  // ���� "x"
#define VIP_FLAG_Y (1<<24)  // ���� "y"
#define VIP_FLAG_Z (1<<25)  // ���� "z"

/* ���������� 1, ���� ����� ���, � ��������� ������ 0 */
native VSGetUserVip(id)

/* ���������� 1, ���� ����� ����� ��������� ����, � ��������� ������ 0 */
native VSGetVipFlag(id, flag)

/* ���������� ����� ������, � ���� ������� ������������������ */
native VSGetVipFlags(id)

/* ������������ ����� �����������
���������: 
	name - �������� �����������
	flag - ���� �������
����������:
	id �����������
*/
native VSRegisterItem(name[], flag)

/* ���������� 1, ���� ���������� �������, � ��������� ������ 0 */
native VSGetItemState(id)

/* ���������� ���������� � ���� */
native VSEnableItem(id)

/* ��������� ���������� � ���� */
native VSDisableItem(id)

/* ������������� �������� ���������� � ���� */
native VSSetItemName(id, name[])

/* ��������� ����� ������ */
native VSAddVipFlags(id, flags)

/* ������� ������ ����������� */
forward VSItemSelected(id, itemid)

/* ������� ����������� ���� */
forward VSVipConnect(id)

/* ������� �������� ���� */
forward VSOpeningMenu(id)

/* ����������� ������ �� �����, � ���� ������� ������������������ */
stock VSStrToFlags(const str[])
{
	new bin = 0;
	new len = strlen(str);
	
	for (new i = 0; i < len; ++i)
	{
		bin |= (1<<(str[i] - 'a'));
	}
	
	return bin;
}

/* ����������� ����� � ������ */
stock VSFlagsToStr(const bits, str[])
{
	new ch[2];
	
	for (new i = 0, len = 0; i < 27; ++i)
	{
		if (bits & (1<<i))
		{
			format(ch, charsmax(ch), "%c", (i + 'a'));
			strcat(str, ch, ++len);
		}
	}
}
