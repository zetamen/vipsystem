/* VSVipPrefixAndGreenChat v1.3

��������:
	��������� ������� [VIP] � ���� � ������
	����� ��������� �������.
���� ������� �� ���������:
	VIP_FLAG_ALL.
��� �����������:
	����������.
*/

#include <amxmodx>
#include <VIPSystem>

#define ACCESS_FLAG VIP_FLAG_ALL

new channels[8][] = 
{
	"#Cstrike_Chat_CT",
	"#Cstrike_Chat_T",
	"#Cstrike_Chat_CT_Dead",
	"#Cstrike_Chat_T_Dead",
	"#Cstrike_Chat_Spec",
	"#Cstrike_Chat_All",
	"#Cstrike_Chat_AllDead",
	"#Cstrike_Chat_AllSpec"
}

new newChannels[8][] =
{
	"(Counter-Terrorist) [VIP] %s1 :  %s2",
	"(Terrorist) [VIP] %s1 :  %s2",
	"*DEAD*(Counter-Terrorist) [VIP] %s1 :  %s2",
	"*DEAD*(Terrorist) [VIP] %s1 :  %s2",
	"(Spectator) [VIP] %s1 :  %s2",
	"[VIP] %s1 :  %s2",
	"*DEAD* [VIP] %s1 :  %s2",
	"*SPEC* [VIP] %s1 :  %s2"
}

new Trie:vipChannels

public plugin_init() 
{
	register_plugin("VSVipPrefixAndGreenChat", "1.3", "ZETA [M|E|N]");
	
	vipChannels = TrieCreate();
	
	for (new i = 0; i < 8; i++)
	{
		TrieSetString(vipChannels, channels[i], newChannels[i]);
	}
	
	register_message(get_user_msgid("SayText"), "MessageSayText");
}

public MessageSayText(msgid, msgdest, id)
{
	new channel[64];
	get_msg_arg_string(2, channel, charsmax(channel));
	
	if(!TrieGetString(vipChannels, channel, channel, charsmax(channel)))
	{
		return;
	}

	new sender = get_msg_arg_int(1);

	if(sender && VSGetVipFlag(sender, ACCESS_FLAG))
	{
		set_msg_arg_string(2, channel);
	}
}

public plugin_end()
{
	TrieDestroy(vipChannels);
}
