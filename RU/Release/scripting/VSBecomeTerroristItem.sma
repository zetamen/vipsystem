/* VSBecomeTerroristItem v1.3

��������:
	������������ �� ������� �����������.
���� ������� �� ���������:
	VIP_FLAG_F.
��� �����������:
	����������.
*/

#include <amxmodx>
#include <VIPSystem>
#include <cstrike>
#include <hamsandwich>

#define ACCESS_FLAG VIP_FLAG_F

new curItem;

public plugin_init() 
{
	register_plugin("VSBecomeTerroristItem", "1.3", "ZETA [M|E|N]");
	
	curItem = VSRegisterItem("Become Terrorist", ACCESS_FLAG);
}

public VSItemSelected(id, itemid)
{
	if (itemid == curItem)
	{
		user_silentkill(id);
		cs_set_user_team(id, CS_TEAM_T);
		ExecuteHamB(Ham_CS_RoundRespawn, id);
	}
}
