/* VSWeapons v1.3 

��������:
	������. �� ����� ������ M4A1, AK47, AWP, Galil, Famas, Desert Eagle � �������.
�����������:
	M4A1 ������ ������ CT
	AK47 ������ ������ �����������
�����:
	vs_amount_opening_weapons_menu - ���������� ��� �������� ���� �� ���� �����.
	����������: 0 - �� ����������.
	vs_available_round - c ������ ������ ����� �������� ����.
	����������: 0 - �� ����������.
���� ������� �� ���������:
	VIP_FLAG_Z.
��� �����������:
	����������.
�������:
	vip_weapons - ���� ������ ������.
*/

#include <amxmodx>
#include <VIPSystem>
#include <hamsandwich>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_Z

new curItem;

new round = 0;

new weapons[18] = {CSW_SCOUT, CSW_XM1014, CSW_MAC10, CSW_AUG, CSW_UMP45, CSW_SG550, CSW_GALIL, CSW_FAMAS, CSW_AWP, 
	CSW_MP5NAVY, CSW_M249, CSW_M3, CSW_M4A1, CSW_TMP, CSW_G3SG1, CSW_SG552, CSW_AK47, CSW_P90
}

new pistols[6] = {CSW_P228, CSW_ELITE, CSW_FIVESEVEN, CSW_GALIL, CSW_USP, CSW_GLOCK18}

new cvarAmountOpeningWeaponsMenu, cvarAvailableRound;

new countOpeningMenu[33];

public plugin_init() 
{
	register_plugin("VSWeapons", "1.3", "ZETA [M|E|N]");
	
	cvarAmountOpeningWeaponsMenu = register_cvar("vs_amount_opening_weapons_menu", "1");
	cvarAvailableRound = register_cvar("vs_available_round", "2");
	
	register_clcmd("vip_weapons", "CmdVipWeapons", ADMIN_ALL, "Show Vip Weapons Menu");
	
	register_event("HLTV", "EventNewRound", "a", "1=0", "2=0");
	register_event("TextMsg", "EventRestartRound", "a", "2=#Game_will_restart_in", "2=#Game_Commencing");
	RegisterHam(Ham_Spawn, "player", "EventSpawnPlayer", 1);
	
	curItem = VSRegisterItem("Weapons", ACCESS_FLAG);
}

public CmdVipWeapons(id)
{
	if (VSGetVipFlag(id, ACCESS_FLAG))
	{
		new availableRound = get_pcvar_num(cvarAvailableRound);
		
		if ((availableRound != 0) && (round <= availableRound))
		{
			client_print(id, print_chat, "Weapons can be selected after %d rounds.", availableRound);
			return PLUGIN_HANDLED;
		}
		
		new amountOpeningWeaponsMenu = get_pcvar_num(cvarAmountOpeningWeaponsMenu);

		if ((amountOpeningWeaponsMenu  != 0) && (countOpeningMenu[id] >= amountOpeningWeaponsMenu ))
		{
			client_print(id, print_chat, "The pistols menu can be opened %d times per round", amountOpeningWeaponsMenu);
			return PLUGIN_HANDLED;
		}
		
		countOpeningMenu[id]++;
		
		ShowWeaponsMenu(id);
	}
	
	return PLUGIN_HANDLED;
}

public VSItemSelected(id, itemid)
{
	if ((itemid == curItem))
	{
		new availableRound = get_pcvar_num(cvarAvailableRound);
		
		if ((availableRound != 0) && (round <= availableRound))
		{
			client_print(id, print_chat, "Weapons can be selected after %d rounds.", availableRound);
			return PLUGIN_HANDLED;
		}
		
		new amountOpeningWeaponsMenu = get_pcvar_num(cvarAmountOpeningWeaponsMenu);

		if ((amountOpeningWeaponsMenu  != 0) && (countOpeningMenu[id] >= amountOpeningWeaponsMenu ))
		{
			client_print(id, print_chat, "The pistols menu can be opened %d times per round", amountOpeningWeaponsMenu);
			return PLUGIN_HANDLED;
		}
		
		countOpeningMenu[id]++;
		
		ShowWeaponsMenu(id);
	}
	
	return PLUGIN_HANDLED;
}

StripWeapons(id)
{
	for (new i = 0; i < 18; i++)
	{
		if (fm_strip_user_gun(id, weapons[i]))
		{
			break;
		}
	}
}

public ShowWeaponsMenu(id)
{
	new menu = menu_create("Weapons", "WeaponsMenuHandler");
	
	new team = get_user_team(id);
	
	if (team == 2)
	{
		menu_additem(menu, "Colt M4A1");
	}
	else
	{
		menu_additem(menu, "AK47");
	}
	
	menu_additem(menu, "AWP");
	menu_additem(menu, "Galil");
	menu_additem(menu, "Famas");
	menu_additem(menu, "Desert Eagle");
	menu_additem(menu, "Grenades");
	
	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}

public WeaponsMenuHandler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	
	switch(item)
	{
		case 0:
		{
			new team = get_user_team(id);
			
			StripWeapons(id);
			
			if (team == 2)
			{
				fm_give_item(id, "weapon_m4a1");
				ExecuteHam(Ham_GiveAmmo, id, 90, "556nato", 90);
			}
			else
			{
				fm_give_item(id, "weapon_ak47");
				ExecuteHam(Ham_GiveAmmo, id, 90, "762nato", 90);
			}
		}
		case 1:
		{
			StripWeapons(id);
			fm_give_item(id, "weapon_awp");
			ExecuteHam(Ham_GiveAmmo, id, 30, "338magnum", 30);
		}
		case 2:
		{
			StripWeapons(id);
			fm_give_item(id, "weapon_galil");
			ExecuteHam(Ham_GiveAmmo, id, 90, "556nato", 90);
		}
		case 3:
		{
			StripWeapons(id);
			fm_give_item(id, "weapon_famas");
			ExecuteHam(Ham_GiveAmmo, id, 90, "556nato", 90);
		}
		case 4:
		{
			for (new i = 0; i < 6; i++)
			{
				if (fm_strip_user_gun(id, pistols[i]))
				{
					break;
				}
			}
			
			fm_give_item(id, "weapon_deagle");
			ExecuteHam(Ham_GiveAmmo, id, 35, "50ae", 35);
		}
		case 5:
		{
			fm_give_item(id, "weapon_hegrenade");
			fm_give_item(id, "weapon_flashbang");
			fm_give_item(id, "weapon_flashbang");
			fm_give_item(id, "weapon_smokegrenade");
		}
	}
	
	return PLUGIN_HANDLED;
}

public EventRestartRound()
{
	round = 0;
}

public EventNewRound()
{
	round++;
}

public EventSpawnPlayer(id)
{
	countOpeningMenu[id] = 0;
}

stock fm_give_item(index, const item[]) 
{
	if (!equal(item, "weapon_", 7) && !equal(item, "ammo_", 5) && !equal(item, "item_", 5) && !equal(item, "tf_weapon_", 10))
		return 0;
	
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, item));
	if (!pev_valid(ent))
		return 0;

	new Float:origin[3];
	pev(index, pev_origin, origin);
	set_pev(ent, pev_origin, origin);
	set_pev(ent, pev_spawnflags, pev(ent, pev_spawnflags) | SF_NORESPAWN);
	dllfunc(DLLFunc_Spawn, ent);

	new save = pev(ent, pev_solid);
	dllfunc(DLLFunc_Touch, ent, index);
	if (pev(ent, pev_solid) != save)
		return ent;

	engfunc(EngFunc_RemoveEntity, ent);

	return -1;
}

stock bool:fm_strip_user_gun(index, wid = 0, const wname[] = "") {
	new ent_class[32];
	if (!wid && wname[0])
		copy(ent_class, sizeof ent_class - 1, wname);
	else {
		new weapon = wid, clip, ammo;
		if (!weapon && !(weapon = get_user_weapon(index, clip, ammo)))
			return false;
		
		get_weaponname(weapon, ent_class, sizeof ent_class - 1);
	}

	new ent_weap = fm_find_ent_by_owner(-1, ent_class, index);
	if (!ent_weap)
		return false;

	engclient_cmd(index, "drop", ent_class);

	new ent_box = pev(ent_weap, pev_owner);
	if (!ent_box || ent_box == index)
		return false;

	dllfunc(DLLFunc_Think, ent_box);

	return true;
}

stock fm_find_ent_by_owner(index, const classname[], owner, jghgtype = 0) {
	new strtype[11] = "classname", ent = index;
	switch (jghgtype) {
		case 1: strtype = "target";
		case 2: strtype = "targetname";
	}

	while ((ent = engfunc(EngFunc_FindEntityByString, ent, strtype, classname)) && pev(ent, pev_owner) != owner) {}

	return ent;
}
