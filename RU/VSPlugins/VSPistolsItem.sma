/* VSPistolsItem v1.3 

��������:
	��������� ����� ����� ��������.
�����:
	vs_amount_opening_pistols_menu - ���������� ��� �������� ���� �� ���� �����.
	����������: 0 - �� ����������
���� ������� �� ���������:
	VIP_FLAG_E.
��� �����������:
	����������.
*/

#include <amxmodx>
#include <VIPSystem>
#include <hamsandwich>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_E

new curItem;

new cvarAmountOpeningPistolsMenu;

new pistols[6] = {CSW_P228, CSW_ELITE, CSW_FIVESEVEN, CSW_GALIL, CSW_USP, CSW_GLOCK18}

new countOpeningMenu[33];

public plugin_init() 
{
	register_plugin("VSPistolsItem", "1.3", "ZETA [M|E|N]");
	
	cvarAmountOpeningPistolsMenu = register_cvar("vs_amount_opening_pistols_menu", "1");
	
	RegisterHam(Ham_Spawn, "player", "hamSpawn", 1);
	
	curItem = VSRegisterItem("Pistols", ACCESS_FLAG);
}

public VSItemSelected(id, itemid)
{
	if ((itemid == curItem) && is_user_alive(id))
	{
		new amountOpeningPistolsMenu = get_pcvar_num(cvarAmountOpeningPistolsMenu);

		if ((amountOpeningPistolsMenu != 0) && (countOpeningMenu[id] >= amountOpeningPistolsMenu))
		{
			client_print(id, print_chat, "The pistols menu can be opened %d times per round", amountOpeningPistolsMenu);
			return PLUGIN_HANDLED;
		}
		
		countOpeningMenu[id]++;
	
		ShowPistolsMenu(id);
	}
	
	return PLUGIN_HANDLED;
}

public ShowPistolsMenu(id)
{	
	new menu = menu_create("Pistols", "PistolsMenuHandler");
	
	menu_additem(menu, "USP");
	menu_additem(menu, "Glock");
	menu_additem(menu, "Desert Eagle");
	menu_additem(menu, "228 Compact");
	
	new team = get_user_team(id);
	
	if (team == 2)
	{
		menu_additem(menu, "ES Five-Seven");
	}
	else
	{
		menu_additem(menu, ".40 Dual Elites");
	}
	
	menu_display(id, menu, 0);
	return PLUGIN_HANDLED;
}

public PistolsMenuHandler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	
	if (!is_user_alive(id))
	{
		return PLUGIN_HANDLED;
	}
	
	for (new i = 0; i < 6; i++)
	{
		if (fm_strip_user_gun(id, pistols[i]))
		{
			break;
		}
	}
	
	switch(item)
	{
		case 0:
		{
			fm_give_item(id, "weapon_usp");
			ExecuteHam(Ham_GiveAmmo, id, 100, "45acp", 100);
		}
		case 1:
		{
			fm_give_item(id, "weapon_glock18");
			ExecuteHam(Ham_GiveAmmo, id, 120, "9mm", 120);
		}
		case 2:
		{
			fm_give_item(id, "weapon_deagle");
			ExecuteHam(Ham_GiveAmmo, id, 35, "50ae", 35);
		}
		case 3:
		{
			fm_give_item(id, "weapon_p228");
			ExecuteHam(Ham_GiveAmmo, id, 52, "357sig", 52);
		}
		case 4:
		{
			new team = get_user_team(id);
			
			if (team == 2)
			{
				fm_give_item(id, "weapon_fiveseven");
				ExecuteHam(Ham_GiveAmmo, id, 100, "57mm", 100);
			}
			else
			{
				fm_give_item(id, "weapon_elite");
				ExecuteHam(Ham_GiveAmmo, id, 120, "9mm", 120);
			}
		}
	}
	
	return PLUGIN_HANDLED;
}

public hamSpawn(id)
{
	countOpeningMenu[id] = 0;
}

stock fm_give_item(index, const item[]) 
{
	if (!equal(item, "weapon_", 7) && !equal(item, "ammo_", 5) && !equal(item, "item_", 5) && !equal(item, "tf_weapon_", 10))
		return 0;
	
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, item));
	if (!pev_valid(ent))
		return 0;

	new Float:origin[3];
	pev(index, pev_origin, origin);
	set_pev(ent, pev_origin, origin);
	set_pev(ent, pev_spawnflags, pev(ent, pev_spawnflags) | SF_NORESPAWN);
	dllfunc(DLLFunc_Spawn, ent);

	new save = pev(ent, pev_solid);
	dllfunc(DLLFunc_Touch, ent, index);
	if (pev(ent, pev_solid) != save)
		return ent;

	engfunc(EngFunc_RemoveEntity, ent);

	return -1;
}

stock bool:fm_strip_user_gun(index, wid = 0, const wname[] = "") {
	new ent_class[32];
	if (!wid && wname[0])
		copy(ent_class, sizeof ent_class - 1, wname);
	else {
		new weapon = wid, clip, ammo;
		if (!weapon && !(weapon = get_user_weapon(index, clip, ammo)))
			return false;
		
		get_weaponname(weapon, ent_class, sizeof ent_class - 1);
	}

	new ent_weap = fm_find_ent_by_owner(-1, ent_class, index);
	if (!ent_weap)
		return false;

	engclient_cmd(index, "drop", ent_class);

	new ent_box = pev(ent_weap, pev_owner);
	if (!ent_box || ent_box == index)
		return false;

	dllfunc(DLLFunc_Think, ent_box);

	return true;
}

stock fm_find_ent_by_owner(index, const classname[], owner, jghgtype = 0) {
	new strtype[11] = "classname", ent = index;
	switch (jghgtype) {
		case 1: strtype = "target";
		case 2: strtype = "targetname";
	}

	while ((ent = engfunc(EngFunc_FindEntityByString, ent, strtype, classname)) && pev(ent, pev_owner) != owner) {}

	return ent;
}
