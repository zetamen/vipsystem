/* VSArmorItem1 v1.3

��������:
	���� ���� � ����������.
�����:
	vs_armor_amount - ���������� �����.
���� ������� �� ���������:
	VIP_FLAG_C.
��� �����������:
	����������.
*/

#include <amxmodx>
#include <cstrike>
#include <VIPSystem>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_C

new cvarArmor;

new curItem;

public plugin_init() 
{
	register_plugin("VSArmorItem1", "1.3", "ZETA [M|E|N]");
	
	cvarArmor = register_cvar("vs_armor_amount", "100");
	
	curItem = VSRegisterItem("Armor", ACCESS_FLAG);
}

public VSItemSelected(id, itemid)
{
	if ((itemid == curItem) && is_user_alive(id))
	{
		cs_set_user_armor(id, get_pcvar_num(cvarArmor), CS_ARMOR_VESTHELM);
	}
	
	return PLUGIN_HANDLED;
}
