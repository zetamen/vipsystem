/* VSLongJump v1.5

��������:
	��������� ������ ������� ������.
������������ ������:
	http://forums.alliedmods.net/showpost.php?p=689064&postcount=1806
�����:
	vs_longjump_force - ���� ������.
	vs_longjump_height - ������ ������.
	vs_longjump_cooldown - ����� �����������.
*/

#include <amxmodx>
#include <VIPSystem>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_D

new bool:longJump[33];
new Float:lastLongJumpTime[33];

new cvarLongjumpForce, cvarLongjumpHeight, cvarLongjumpCooldown;

public plugin_init() 
{
	register_plugin("VSLongJump", "1.5", "ZETA [M|E|N]");
	
	register_forward(FM_PlayerPreThink, "EventPlayerPreThink");
	register_event("HLTV", "EventRoundStart", "a", "1=0", "2=0");
	
	cvarLongjumpForce = register_cvar("vs_longjump_force", "550");
	cvarLongjumpHeight = register_cvar("vs_longjump_height", "255");
	cvarLongjumpCooldown = register_cvar("vs_longjump_cooldown", "5.0");
	
	arrayset(longJump, false, 33);
}

public client_putinserver(id)
{
	set_task(1.0, "CheckFlag", id);
}

public CheckFlag(id)
{
	if (VSGetVipFlag(id, ACCESS_FLAG))
	{
		longJump[id] = true;
	}
}

public client_disconnect(id)
{
	longJump[id] = false;
}
	
public EventPlayerPreThink(id)
{
	if (!is_user_alive(id))
	{
		return FMRES_IGNORED;
	}
	
	if (allow_LongJump(id))
	{
		static Float:velocity[3];
		velocity_by_aim(id, get_pcvar_num(cvarLongjumpForce), velocity);
		
		velocity[2] = get_pcvar_float(cvarLongjumpHeight);
		
		set_pev(id, pev_velocity, velocity);
		
		lastLongJumpTime[id] = get_gametime();
	}
	
	return FMRES_IGNORED;
}

allow_LongJump(id)
{
	static buttons;
	buttons = pev(id, pev_button);
	
	if (!(buttons & IN_JUMP) || !(buttons & IN_DUCK))
		return false;
	
	if (!longJump[id])
		return false;
	
	if (!(pev(id, pev_flags) & FL_ONGROUND) || fm_get_speed(id) < 80)
		return false;

	if (get_gametime() - lastLongJumpTime[id] < get_pcvar_float(cvarLongjumpCooldown))
		return false;
	
	return true;
}

stock fm_get_speed(entity)
{
	static Float:velocity[3];
	pev(entity, pev_velocity, velocity);
	
	return floatround(vector_length(velocity));
}
