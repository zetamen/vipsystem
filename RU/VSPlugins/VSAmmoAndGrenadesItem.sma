/* VSAmmoAndGrenadesItem v1.3 

��������:
	���� ������� � ��� �������.
���� ������� �� ���������:
	VIP_FLAG_D.
��� �����������:
	����������.
*/

#include <amxmodx>
#include <VIPSystem>
#include <hamsandwich>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_D

new const MAXBPAMMO[] = {-1, 52, -1, 90, 1, 32, 1, 100, 90, 1, 120, 100, 100, 90, 90, 90, 100, 120,
		30, 120, 200, 32, 90, 120, 90, 2, 35, 90, 90, -1, 100};

new const AMMOTYPE[][] = {"", "357sig", "", "762nato", "", "buckshot", "", "45acp", "556nato", "", "9mm", 
		"57mm", "45acp", "556nato", "556nato", "556nato", "45acp", "9mm", "338magnum", "9mm", "556natobox", 
		"buckshot", "556nato", "9mm", "762nato", "", "50ae", "556nato", "762nato", "", "57mm"};

new curItem;

public plugin_init() 
{
	register_plugin("VSAmmoAndGrenadesItem", "1.3", "ZETA [M|E|N]");
	
	curItem = VSRegisterItem("Ammo and grenades", ACCESS_FLAG);
}

public VSItemSelected(id, itemid)
{
	if ((itemid == curItem) && is_user_alive(id))
	{
		new weapons[32], num;
		get_user_weapons(id, weapons, num);
		
		for (new i = 0; i < num; i++)
		{
			ExecuteHam(Ham_GiveAmmo, id, MAXBPAMMO[weapons[i]], AMMOTYPE[weapons[i]], MAXBPAMMO[weapons[i]]);
		}

		fm_give_item(id, "weapon_hegrenade");
		fm_give_item(id, "weapon_flashbang");
		fm_give_item(id, "weapon_flashbang");
		fm_give_item(id, "weapon_smokegrenade");
	}
	
	return PLUGIN_HANDLED;
}

stock fm_give_item(index, const item[]) 
{
	if (!equal(item, "weapon_", 7) && !equal(item, "ammo_", 5) && !equal(item, "item_", 5) && !equal(item, "tf_weapon_", 10))
		return 0;
	
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, item));
	if (!pev_valid(ent))
		return 0;

	new Float:origin[3];
	pev(index, pev_origin, origin);
	set_pev(ent, pev_origin, origin);
	set_pev(ent, pev_spawnflags, pev(ent, pev_spawnflags) | SF_NORESPAWN);
	dllfunc(DLLFunc_Spawn, ent);

	new save = pev(ent, pev_solid);
	dllfunc(DLLFunc_Touch, ent, index);
	if (pev(ent, pev_solid) != save)
		return ent;

	engfunc(EngFunc_RemoveEntity, ent);

	return -1;
}
