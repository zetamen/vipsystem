/* VSVipSpeedItem v1.3 

��������:
	������������� �������� � ����� �������.
�����:
	vs_vip_speed - �������� ����.
���� ������� �� ���������:
	VIP_FLAG_B.
��� �����������:
	����������.
*/

#include <amxmodx>
#include <VIPSystem>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_B

new cvarVipSpeed;
new bool:speed[33];

new curItem;

public plugin_init() 
{
	register_plugin("VSVipSpeedItem", "1.3", "ZETA [M|E|N]");
	
	cvarVipSpeed = register_cvar("vs_vip_speed", "290");
	
	curItem = VSRegisterItem("Speed", ACCESS_FLAG);
	
	register_event("HLTV", "EventRoundStart", "a", "1=0", "2=0");
	register_event("CurWeapon", "EventCurWeapon", "be", "1=1");
}

public VSItemSelected(id, itemid)
{
	if (itemid == curItem)
	{
		speed[id] = true;
	}
}

public EventRoundStart()
{
	arrayset(speed, false, 33);
}

public EventCurWeapon(id)
{
	if (speed[id])
	{
		fm_set_user_maxspeed(id, get_pcvar_float(cvarVipSpeed));
	}
}

stock fm_set_user_maxspeed(index, Float:speed = -1.0) 
{
	engfunc(EngFunc_SetClientMaxspeed, index, speed);
	set_pev(index, pev_maxspeed, speed);
	return 1;
}
