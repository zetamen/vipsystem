/* VSSpawnArmor v1.3 

Description:
	Gives armor in respawn.
Cvars:
	vs_spawn_armor_amount - armor amount.
Access flag by default:
	VIP_FLAG_B.
Type:
	Constant.
*/

#include <amxmodx>
#include <VIPSystem>
#include <hamsandwich>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_B

new cvarArmor;

public plugin_init() 
{
	register_plugin("VSSpawnArmor", "1.3", "ZETA [M|E|N]");
	
	cvarArmor = register_cvar("vs_spawn_armor_amount", "100");
	
	RegisterHam(Ham_Spawn, "player", "EventSpawn", 1);
}

public EventSpawn(id)
{
	if (VSGetVipFlag(id, ACCESS_FLAG) && is_user_alive(id))
	{
		fm_set_user_armor(id, get_pcvar_num(cvarArmor));
	}
}

stock fm_set_user_armor(index, armor) {
	set_pev(index, pev_armorvalue, float(armor));
	return 1;
}
