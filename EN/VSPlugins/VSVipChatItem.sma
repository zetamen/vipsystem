/* VSVipChatItem v1.5

Description:
	Adds a private chat for vips
Commands:
	say_vip - say in VIP chat.
Note:
	bind "<button>" "messagemode say_vip"
Type:
	Item
*/

#include <amxmodx>
#include <VIPSystem>

#define ACCESS_FLAG VIP_FLAG_F

new msgidSayText, maxPlayers;
new curItem;

public plugin_init() 
{
	register_plugin("VSVipChatItem", "1.5", "ZETA [M|E|N]");
	
	msgidSayText = get_user_msgid("SayText");
	maxPlayers = get_maxplayers();
	
	curItem = VSRegisterItem("Vip Chat", ACCESS_FLAG);
	
	register_clcmd("say_vip", "ClcmdSayVip", ADMIN_ALL, "");
}

public VSItemSelected(id, itemid)
{
	if (itemid == curItem)
	{
		client_cmd(id, "messagemode say_vip");
	}
}

public ClcmdSayVip(id)
{
	if (!VSGetVipFlag(id, ACCESS_FLAG))
	{
		return PLUGIN_HANDLED;
	}
	
	new name[32];
	get_user_name(id, name, charsmax(name));
	
	new message[64]; 
	read_args(message, charsmax(message)); 
	remove_quotes(message);
	
	new chat[64];
	if (is_user_alive(id))
	{
		chat = "(VIP) %s1 :  %s2";
	}
	else
	{
		chat = "*DEAD*(VIP) %s1 :  %s2";
	}
	
	ClientPrint(chat, name, message);
	return PLUGIN_HANDLED;
}

public ClientPrint(chat[], name[], message[])
{
	for (new id = 1; id <= maxPlayers; ++id)
	{
		if (VSGetVipFlag(id, ACCESS_FLAG))
		{
			message_begin(MSG_ONE, msgidSayText, _, id);
			write_byte(id);
			write_string(chat);
			write_string(name);
			write_string(message);
			message_end();
		}
	}
}
