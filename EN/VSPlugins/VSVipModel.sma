/* VSVipModel v1.5

Description:
	Change vip model
Notes:
	Add model:
	models/player/vip_ct/vip_ct.mdl
	models/player/vip_ter/vip_ter.mdl
*/

#include <amxmodx>
#include <cstrike>
#include <VIPSystem>

#define ACCESS_FLAG VIP_FLAG_M

public plugin_init() 
{
	register_plugin("VSVipModel", "1.5", "swds.dll");
	register_event("ResetHUD", "ResetModel", "b");
}

public plugin_precache() 
{
        precache_model("models/player/vip_ct/vip_ct.mdl")
        precache_model("models/player/vip_ter/vip_ter.mdl")
}

public ResetModel(id, level, cid)
{
	if (is_user_alive(id) && VSGetVipFlag(id, ACCESS_FLAG))
	{
		new CsTeams:userTeam = cs_get_user_team(id)
		
		if (userTeam == CS_TEAM_T) 
		{
			cs_set_user_model(id, "vip_ter")
		}
		else if(userTeam == CS_TEAM_CT) 
		{
			cs_set_user_model(id, "vip_ct")
		}
		else 
		{
			cs_reset_user_model(id)
		}
	}
}
