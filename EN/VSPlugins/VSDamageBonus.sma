/* VSDamageBonus v1.5

Description:
	added bonus for damage
Cvars:
	vs_damage_bonus_on - enable / disable bonuses
	vs_max_health - maximum vip health
	vs_hit_money - money amount for hit
	vs_kill_health - health amount for kill
	vs_head_kill_health - health amount for headshot
	vs_kill_money - money amount for kill
	vs_head_kill_money - money amount for headshot
*/

#include <amxmodx>
#include <hamsandwich>
#include <fakemeta>
#include <cstrike>
#include <VIPSystem>

#define ACCESS_FLAG VIP_FLAG_A

new cvarMaxHealth, cvarDamageBonusOn, cvarHitMoney, cvarKillHealth, 
cvarHeadKillHealth, cvarKillMoney, cvarHeadKillMoney;

public plugin_init() 
{
	register_plugin("VSDamageBonus", "1.5", "ZETA [M|E|N]");
	RegisterHam(Ham_Spawn, "player", "PlayerSpawn", 1);
	RegisterHam(Ham_TakeDamage, "player", "PlayerTakeDamage", 1);
	RegisterHam(Ham_Killed, "player", "PlayerKilled");
	
	cvarDamageBonusOn = register_cvar("vs_damage_bonus_on", "1");
	cvarMaxHealth = register_cvar("vs_max_health", "150");
	cvarHitMoney = register_cvar("vs_hit_money", "3");
	cvarKillHealth = register_cvar("vs_kill_health", "15");
	cvarHeadKillHealth = register_cvar("vs_head_kill_health", "30");
	cvarKillMoney = register_cvar("vs_kill_money", "500");
	cvarHeadKillMoney = register_cvar("vs_head_kill_money", "800");
}

public PlayerSpawn(id)
{
	if (!get_pcvar_num(cvarDamageBonusOn) || !VSGetVipFlag(id, ACCESS_FLAG))
	{
		return;		
	}
	
	set_pev(id, pev_health, get_pcvar_float(cvarMaxHealth));
}

public PlayerTakeDamage(victim, weapon, attacker, Float:damage, damage_type)
{
	if ((attacker == victim) || 
		!get_pcvar_num(cvarDamageBonusOn) || 
		!VSGetVipFlag(attacker, ACCESS_FLAG))
	{
		return;		
	}
	
	cs_set_user_money(attacker, cs_get_user_money(attacker) + get_pcvar_num(cvarHitMoney));
}

public PlayerKilled(victim, attacker, corpse)
{
	if (!get_pcvar_num(cvarDamageBonusOn) || !VSGetVipFlag(attacker, ACCESS_FLAG))
	{
		return;		
	}
	
	new weapon, hitzone;
	get_user_attacker(victim, weapon, hitzone);
	
	if (hitzone == HIT_HEAD)
	{
		AddHealth(attacker, get_pcvar_num(cvarHeadKillHealth));
		cs_set_user_money(attacker, cs_get_user_money(attacker) + get_pcvar_num(cvarHeadKillMoney));
	}
	else
	{
		AddHealth(attacker, get_pcvar_num(cvarKillHealth));
		cs_set_user_money(attacker, cs_get_user_money(attacker) + get_pcvar_num(cvarKillMoney));
	}
}

AddHealth(id, amount)
{
	new health = pev(id, pev_health) + amount;
	
	if (health > get_pcvar_num(cvarMaxHealth))
	{
		health = get_pcvar_num(cvarMaxHealth);
	}
	
	set_pev(id, pev_health, float(health));
}
