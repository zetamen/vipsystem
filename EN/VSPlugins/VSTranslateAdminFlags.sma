/* VSTranslateAdminFlags v1.5

Description:
	Translates admins flags in the flags of VS
Cvars:
	vs_translate_flags - Defines the rules translate flags
Notes:
	Format rule: <admin flags> <system flags>;
	You can register multiple rules.
*/

#include <amxmodx>
#include <VIPSystem>

new cvarTranslateFlags;

new Array:adminFlagsArray;
new Array:vsFlagsArray;
new translateNumber = 0;

public plugin_init() 
{
	register_plugin("VSTranslateAdminFlags", "1.5", "ZETA [M|E|N]");
	
	cvarTranslateFlags = register_cvar("vs_translate_flags", "m a;sqrt abcdefghijklmnopqrstuvwxyz;");
}

public plugin_precache()
{
	adminFlagsArray = ArrayCreate(1, 1);
	vsFlagsArray = ArrayCreate(1, 1);
}

public plugin_cfg()
{
	new left[200], right[200];
	get_pcvar_string(cvarTranslateFlags, right, charsmax(right));
	
	new adminFlags[27], vsFlags[27];
	do
	{
		strtok(right, left, charsmax(left), right, charsmax(right), ';');
		
		parse(left, adminFlags, charsmax(adminFlags), vsFlags, charsmax(vsFlags));
		ArrayPushCell(adminFlagsArray, read_flags(adminFlags));
		ArrayPushCell(vsFlagsArray, VSStrToFlags(vsFlags));
		++translateNumber;
		
	}
	while(strlen(right) > 0)
}

public client_putinserver(id)
{
	set_task(1.0, "TranslateFlags", id);
}

public client_infochanged(id)
{
	new newname[32], oldname[32];
	get_user_name(id, oldname, charsmax(oldname));
	get_user_info(id, "name", newname, charsmax(newname));
	
	if (!equal(newname, oldname))
	{
		TranslateFlags(id);
	}
}

public TranslateFlags(id)
{
	new adminFlags, vsFlags;
	for (new i = 0; i < translateNumber; i++)
	{
		adminFlags = ArrayGetCell(adminFlagsArray, i);
		vsFlags = ArrayGetCell(vsFlagsArray, i);
		
		if (get_user_flags(id) & adminFlags)
		{
			VSAddVipFlags(id, vsFlags);
		}
	}
}
