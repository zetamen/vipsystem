/* VSSpawnGrenades v1.3 

Description:
	Gives all grenades in respawn.
Access flag by default:
	VIP_FLAG_B.
Type:
	Constant.
*/

#include <amxmodx>
#include <VIPSystem>
#include <hamsandwich>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_B

public plugin_init() 
{
	register_plugin("VSSpawnGrenades", "1.3", "ZETA [M|E|N]");
	
	RegisterHam(Ham_Spawn, "player", "Spawn", 1);
}

public Spawn(id)
{
	if (VSGetVipFlag(id, ACCESS_FLAG) && is_user_alive(id))
	{
		fm_give_item(id, "weapon_hegrenade");
		fm_give_item(id, "weapon_flashbang");
		fm_give_item(id, "weapon_flashbang");
		fm_give_item(id, "weapon_smokegrenade");
	}
}

stock fm_give_item(index, const item[]) 
{
	if (!equal(item, "weapon_", 7) && !equal(item, "ammo_", 5) && !equal(item, "item_", 5) && !equal(item, "tf_weapon_", 10))
		return 0;
	
	new ent = engfunc(EngFunc_CreateNamedEntity, engfunc(EngFunc_AllocString, item));
	if (!pev_valid(ent))
		return 0;

	new Float:origin[3];
	pev(index, pev_origin, origin);
	set_pev(ent, pev_origin, origin);
	set_pev(ent, pev_spawnflags, pev(ent, pev_spawnflags) | SF_NORESPAWN);
	dllfunc(DLLFunc_Spawn, ent);

	new save = pev(ent, pev_solid);
	dllfunc(DLLFunc_Touch, ent, index);
	if (pev(ent, pev_solid) != save)
		return ent;

	engfunc(EngFunc_RemoveEntity, ent);

	return -1;
}
