/* VSVipSpeed v1.3

Description:
	Sets the vip speed with any weapon.
Cvars:
	vs_vip_speed - vip speed.
Access flag by default:
	VIP_FLAG_C.
Type:
	Constant.
*/

#include <amxmodx>
#include <VIPSystem>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_C

new cvarVipSpeed;

public plugin_init() 
{
	register_plugin("VSVipSpeed", "1.3", "ZETA [M|E|N]");
	
	cvarVipSpeed = register_cvar("vs_vip_speed", "400");
	
	register_event("CurWeapon", "EventCurWeapon", "be", "1=1");
}

public EventCurWeapon(id)
{
	if (VSGetVipFlag(id, ACCESS_FLAG))
	{
		fm_set_user_maxspeed(id, get_pcvar_float(cvarVipSpeed));
	}
}

stock fm_set_user_maxspeed(index, Float:speed = -1.0) 
{
	engfunc(EngFunc_SetClientMaxspeed, index, speed);
	set_pev(index, pev_maxspeed, speed);
	return 1;
}
