[en]
LOADED_VIP = Loaded 1 vip from database
LOADED_VIPS = Loaded %d vips from database
NO_VIPS = No vips found
NO_ENTRY = You have no entry to the server...
NO_VIPS_FILE = File 'addons/amxmodx/configs/vips.ini' not found.
VIPS_LIST = Vips List
OUT_OF_RANGE = Player index out of range (%d).
INVALID_FORMAT = Invalid format registration (%s).
NO_ACC_COM = You have no access to that command.
NO_ITEMS = Items were not added to the menu.
FORMAT_ADD = Format: addvip <Name|IP|Steam> <Password> <access flags> <account flags> <Expired date (dd.mm.yy)>
MENU_OPENING_AVAILABLE = The menu can be opened %d times per round.
VIP_ADDED = VIP is added.
VIP_MENU = Vip Menu
MENU_NEXT = Next
MENU_BACK = Back
MENU_EXIT = Exit

[ru]
LOADED_VIP = Loaded 1 vip from database
LOADED_VIPS = Loaded %d vips from database
NO_VIPS = No vips found
NO_ENTRY = You have no entry to the server...
NO_VIPS_FILE = File 'addons/amxmodx/configs/vips.ini' not found.
VIPS_LIST = Vips List
OUT_OF_RANGE = Player index out of range (%d).
INVALID_FORMAT = Invalid format registration (%s).
NO_ACC_COM = You have no access to that command.
NO_ITEMS = Дополнения не были добавлены в меню.
FORMAT_ADD = Формат: addvip <Имя|IP|Steam> <Пароль> <Флаги доступа> <Флаги аккаунта> <Дата окончания (дд.мм.гг)>
MENU_OPENING_AVAILABLE = Меню можно открыть только %d раз за раунд.
VIP_ADDED = VIP добавлен.
VIP_MENU = Меню
MENU_NEXT = Дальше
MENU_BACK = Назад
MENU_EXIT = Выход

[ro]
LOADED_VIP = 1 VIP incarcat din fisier
LOADED_VIPS = %d VIP-uri incarcate din fisier
NO_VIPS = Nici un VIP nu a fost gasit
NO_VIPS_FILE = Fisierul 'addons/amxmodx/configs/vips.ini' nu a fost gasit
VIPS_LIST = Lista VIP-urilor
OUT_OF_RANGE = Index-ul jucatorului in afara razei de actiune (%d).
INVALID_FORMAT = Format invalid de inregistrare (%s).
NO_ACC_COM = Nu ai acces la comanda.
NO_ITEMS = Itemele nu au fost adaugate la meniu.
FORMAT_ADD = Format: addvip <nume|ip|steam> <parola> <accese> <flaguri>
MENU_OPENING_AVAILABLE = Meniul poate fi deschis de %d ori pe runda .
VIP_ADDED = VIP-ul a fost adaugat.
VIP_MENU = Meniu VIP
VIP_NEXT = Urmatorul
VIP_BACK = Inapoi
VIP_EXIT = Iesire

[lv]
LOADED_VIP = Ieladets 1 VIP's no datubazes
LOADED_VIPS = Ieladeti %d VIP'i no datubazes
NO_VIPS = Neviens VIP's netika atrasts
NO_ENTRY = Tev nav pieejas ieiet serveri
NO_VIPS_FILE = Fails 'addons/amxmodx/configs/vips.ini' netika atrasts.
VIPS_LIST = VIP saraksts
OUT_OF_RANGE = Speletaja indekss ir arpus diapazona (%d).
INVALID_FORMAT = Nederigs formats registracijai (%s).
NO_ACC_COM = Tev ir liegta pieeja sai darbibai.
NO_ITEMS = Preces netika pievienotas izvelnei.
FORMAT_ADD = Formats: addvip <Niks|IP|Steam> <Parole> <Piekluves karogi> <Lietotaja karogi> <Termins beidzies (dd.mm.gg)>
MENU_OPENING_AVAILABLE = Izvelne var tikt atverta %d reizes, katru raundu.
VIP_ADDED = VIP's ir pievienots.
VIP_MENU = VIP izvelne
MENU_NEXT = Nakosais
MENU_BACK = Atpakal
MENU_EXIT = Iziet

[pt]
LOADED_VIP = Carregado 1 VIP na base de dados
LOADED_VIPS = Carregados %d VIPS na base de dados
NO_VIPS = Nao foram encontrados VIPS
NO_ENTRY = Nao entrou no servidor...
NO_FILE = Ficheiro '%s' nao encontrado.
VIPS_LIST = Lista Vips
OUT_OF_RANGE = Jogador fora do intervalo (%d).
INVALID_FORMAT = Formato de registro invalido (%s).
NO_ACC_COM = Nao tens acesso a este comando.
NO_ITEMS = Os items nao estao adicionados ao menu.
FORMAT_ADD = Formato: addvip <Name|IP|Steam> <Password> <access flags> <account flags> <Expired date (dd.mm.yy)>
MENU_OPENING_AVAILABLE = O menu pode ser aberto %d por rounda
VIP_ADDED = VIP adicionado.
VIP_MENU = Vip Menu
MENU_NEXT = Proximo
MENU_BACK = Anterior
MENU_EXIT = Sair