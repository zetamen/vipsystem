/* VSBulletDamage v1.3 

Description:
	Shows the damage done.
Access flag by default:
	VIP_FLAG_M.
Type:
	Constant.
*/

#include <amxmodx>
#include <VIPSystem>
#include <hamsandwich>

#define ACCESS_FLAG VIP_FLAG_M

public plugin_init() 
{
	register_plugin("VSBulletDamage", "1.3", "ZETA [M|E|N]");
	
	RegisterHam(Ham_TakeDamage, "player", "TakeDamage");
}

public TakeDamage(idvictim, idinflictor, idattacker, Float:damage, damagebits)
{
	if (!is_user_alive(idattacker) || (get_user_team(idvictim) == get_user_team(idattacker)))
	{
		return;
	}
	
	if (VSGetVipFlag(idattacker, ACCESS_FLAG))
	{
		set_hudmessage(180, 180, 180, 0.51, 0.51, 0, 6.0, 3.0, _, _, 3);
		show_hudmessage(idattacker, "%d", floatround(damage));
	}
}
