/* VSCTBombRadar v1.3 

Description:
	Shows the location of the bomb for CT on the radar.
Access flag by default:
	VIP_FLAG_T.
Type:
	Constant.
*/

#include <amxmodx>
#include <VIPSystem>

#define ACCESS_FLAG VIP_FLAG_T

new msgidHostagePos, msgidHostageK;
new maxPlayers;

public plugin_init() 
{
	register_plugin("VSCTBombRadar", "1.3", "ZETA [M|E|N]");
	
	msgidHostagePos = get_user_msgid("HostagePos");
	msgidHostageK = get_user_msgid("HostageK");
	
	maxPlayers = get_maxplayers();
	
	set_task(1.0, "UpdateRadar", _, _, _, "b");
}

public UpdateRadar()
{
	static bombOrigin[3], id;
	for (id = 1; id <= maxPlayers; id++)
	{
		if (user_has_weapon(id, CSW_C4))
		{
			get_user_origin(id, bombOrigin);
			break;
		}
	}
	
	for (id = 1; id <= maxPlayers; id++)
	{
		if (VSGetVipFlag(id, ACCESS_FLAG) && (get_user_team(id) == 2) && is_user_alive(id))
		{
			message_begin(MSG_ONE_UNRELIABLE, msgidHostagePos, {0, 0, 0}, id);
			write_byte(id);
			write_byte(0);
			write_coord(bombOrigin[0]);
			write_coord(bombOrigin[1]);
			write_coord(bombOrigin[2]);
			message_end();
		
			message_begin(MSG_ONE_UNRELIABLE, msgidHostageK, {0, 0, 0}, id);
			write_byte(0);
			message_end();
		}
	}
}
