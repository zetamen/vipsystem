/* VSIncreaseScoreItem v1.3

Description:
	Increase score.
�����:
	vs_amount_score - points amount which is added to the account.
Access flag by default:
	VIP_FLAG_G.
Type:
	Selectable.
*/

#include <amxmodx>
#include <VIPSystem>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_G

new cvarAmountScore;
new msgidScoreInfo;

new curItem;

public plugin_init() 
{
	register_plugin("VSIncreaseScoreItem", "1.3", "ZETA [M|E|N]");
	cvarAmountScore = register_cvar("vs_amount_score", "2");
	
	curItem = VSRegisterItem("Increase Score", ACCESS_FLAG);
	
	msgidScoreInfo = get_user_msgid("ScoreInfo");
}

public VSItemSelected(id, itemid)
{
	if (itemid == curItem)
	{
		set_user_frags(id, get_user_frags(id) + get_pcvar_num(cvarAmountScore));
	}
}

stock set_user_frags(id, frags)
{
	set_pev(id, pev_frags, float(frags));
	
	new deaths = get_user_deaths(id);
	new team = get_user_team(id);
		
	message_begin(MSG_ALL, msgidScoreInfo);
	write_byte(id);
	write_short(frags);
	write_short(deaths);
	write_short(0);
	write_short(team);
	message_end();
}
