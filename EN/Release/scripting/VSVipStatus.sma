/* VSVipStatus v1.3 

Description:
	Show vip status in table.
Access flag by default:
	VIP_FLAG_ALL.
Type:
	Constant.
*/

#include <amxmodx>
#include <VIPSystem>

#define VIP_FLAG VIP_FLAG_ALL

public plugin_init()
{
	register_plugin("VSVipStatus", "1.3", "swds.dll")
	register_event("ResetHUD", "ResetHUD", "be")
}

public ResetHUD(id)
{
	set_task(0.5, "VIP", id + 6910)
}

public VIP(TaskID)
{
	new id = TaskID - 6910
	
	if(VSGetVipFlag(id, VIP_FLAG))
	{
		message_begin(MSG_ALL, get_user_msgid("ScoreAttrib"))
		write_byte(id)
		write_byte(4)
		message_end()
	}
}
