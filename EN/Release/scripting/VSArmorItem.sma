/* VSArmorItem v1.3

Description:
	Gives armor.
�����:
	vs_armor_amount - ���������� �����.
Access flag by default:
	VIP_FLAG_C.
Type:
	Selectable.
*/

#include <amxmodx>
#include <VIPSystem>
#include <fakemeta>

#define ACCESS_FLAG VIP_FLAG_C

new cvarArmor;

new curItem;

public plugin_init() 
{
	register_plugin("VSArmorItem", "1.3", "ZETA [M|E|N]");
	
	cvarArmor = register_cvar("vs_armor_amount", "100");
	
	curItem = VSRegisterItem("Armor", ACCESS_FLAG);
}

public VSItemSelected(id, itemid)
{
	if ((itemid == curItem) && is_user_alive(id))
	{
		fm_set_user_armor(id, get_pcvar_num(cvarArmor));
	}
	
	return PLUGIN_HANDLED;
}

stock fm_set_user_armor(index, armor) {
	set_pev(index, pev_armorvalue, float(armor));
	return 1;
}
