/* VSReserveSlot v1.3

Description:
	Allows to add reserve slots for vips.
�����:
	vs_amount_slots - ���������� ��������� ������
Access flag by default:
	VIP_FLAG_A.
Type:
	Constant.
*/

#include <amxmodx>
#include <VIPSystem>

#define ACCESS_FLAG VIP_FLAG_A

new cvarAmountSlots;

public plugin_init() 
{
	register_plugin("VSReserveSlot", "1.3", "ZETA [M|E|N]");
	
	cvarAmountSlots = register_cvar("vs_amount_slots", "1");
}

public client_putinserver(id)
{
	if (get_pcvar_num(cvarAmountSlots) > 0)
	{
		CheckAccess(id);
	}
}

public CheckAccess(id)
{
	new maxplayers = get_maxplayers();
	new players = get_playersnum(1);
	new limit = maxplayers - get_pcvar_num(cvarAmountSlots);
	
	if (VSGetVipFlag(id, ACCESS_FLAG) || (players <= limit))
	{
		return PLUGIN_CONTINUE;
	}
	
	server_cmd("kick #%d ^"Dropped due to slot reservation^"", get_user_userid(id));
	return PLUGIN_HANDLED;
}
