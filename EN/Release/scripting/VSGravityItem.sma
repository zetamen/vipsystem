/* VSGravityItem v1.3 

Description:
	Set gravity.
Access flag by default:
	VIP_FLAG_C.
Type:
	Selectable.
*/

#include <amxmodx>
#include <VIPSystem>
#include <fun>

#define ACCESS_FLAG VIP_FLAG_C

new cvarGravity;

new curItem;

public plugin_init() 
{
	register_plugin("VSGravityItem", "1.3", "MIREX|ZETA");
	cvarGravity = register_cvar("vs_gravity", "0.6");
	
	curItem = VSRegisterItem("Gravity", ACCESS_FLAG);
}

public VSItemSelected(id, itemid)
{
	if (itemid == curItem)
	{
		set_user_gravity(id, get_pcvar_float(cvarGravity));
	}
}
