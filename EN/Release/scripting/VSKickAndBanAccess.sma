/* VSKickAndBanAccess v1.3 

Description:
	Allows you to kick and ban players.
Access flag by default:
	VIP_FLAG_A.
Type:
	Constant.
*/

#include <amxmodx>
#include <VIPSystem>

#define ACCESS_FLAG VIP_FLAG_A

new curItem1, curItem2;

public plugin_init() {
	register_plugin("VSKickAndBanAccess", "1.3", "ZETA [M|E|N]");
	
	curItem1 = VSRegisterItem("Kick Menu", ACCESS_FLAG);
	curItem2 = VSRegisterItem("Ban Menu", ACCESS_FLAG);
}

public VSItemSelected(id, itemid)
{
	if (itemid == curItem1)
	{
		client_cmd(id, "amx_kickmenu");
	}
	else if (itemid == curItem2)
	{
		client_cmd(id, "amx_banmenu");
	}
	
	return PLUGIN_HANDLED;
}

public VSVipConnect(id)
{
	if (VSGetVipFlag(id, ACCESS_FLAG))
	{
		set_user_flags(id, ADMIN_BAN | ADMIN_KICK);
	}
}
