/* VIPSystemExpansion v1.5 */

#include <amxmodx>
#include <VIPSystem>

new cvarAmountOpeningMenu;

new countOpeningMenu[33];

public plugin_init() 
{
	register_plugin("VIPSystemExpansion", "1.5", "ZETA [M|E|N]");
	
	register_event("HLTV", "EventNewRound", "a", "1=0", "2=0");
	
	cvarAmountOpeningMenu = register_cvar("vs_amount_opening_menu", "0");
	
	register_clcmd("say /vipmenu", "ShowVipMenu", ADMIN_ALL, "Show Vip Menu");
	register_clcmd("say /vm", "ShowVipMenu", ADMIN_ALL, "Show Vip Menu");
	register_clcmd("say /menu", "ShowVipMenu", ADMIN_ALL, "Show Vip Menu");
}

public ShowVipMenu(id)
{
	client_cmd(id, "vip_menu");
}

public EventNewRound(id)
{
	arrayset(countOpeningMenu, 0, 33);
}

public VSOpeningMenu(id)
{
	new amountOpeningMenu = get_pcvar_num(cvarAmountOpeningMenu);
	
	if (amountOpeningMenu && (countOpeningMenu[id] >= amountOpeningMenu))
	{
		client_print(id, print_chat, "%L", id, "MENU_OPENING_AVAILABLE", amountOpeningMenu);
		return PLUGIN_HANDLED;
	}
	
	countOpeningMenu[id]++;
	return PLUGIN_CONTINUE;
}
