/* VSAdditionalLivesItem v1.3 

Description:
	Gives additional lives.
Access flag by default:
	VIP_FLAG_E.
Type:
	Selectable.
*/

#include <amxmodx>
#include <VIPSystem>
#include <hamsandwich>

#define ACCESS_FLAG VIP_FLAG_E

new cvarAdditionalLivesAmount;
new additionalLives[33];

new curItem;

public plugin_init() 
{
	register_plugin("VSAdditionalLivesItem", "1.3", "ZETA [M|E|N]");
	
	curItem = VSRegisterItem("Additional Lives", ACCESS_FLAG);
	
	cvarAdditionalLivesAmount = register_cvar("vs_additional_lives_amount", "2");
	
	register_event("HLTV", "EventRoundStart", "a", "1=0", "2=0");
	register_event("DeathMsg", "EventDeath", "a")
}

public VSItemSelected(id, itemid)
{
	if (itemid == curItem)
	{
		additionalLives[id] = get_pcvar_num(cvarAdditionalLivesAmount);
	}
}

public EventRoundStart()
{
	arrayset(additionalLives, 0, 33);
}

public EventDeath()
{
	new victim = read_data(2);
	
	if (additionalLives[victim])
	{
		set_task(0.5, "spawn", victim);
		
		--additionalLives[victim];
	}
}

public spawn(id)
{
	ExecuteHamB(Ham_CS_RoundRespawn, id);
}