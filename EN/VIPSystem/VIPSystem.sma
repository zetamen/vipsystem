/* VIPSystem v1.5 */

#include <amxmodx>

#define VIP_STEAM 	(1<<0)
#define VIP_IP 		(1<<1)
#define VIP_NAME 	(1<<2)

new Array:vipAuthArray;
new Array:vipPasswordArray;
new Array:vipAccessFlagsArray;
new Array:vipAccountFlagsArray;

new cvarPasswordField;

new forwardVipConnect;

new vipFlags[33];

new maxPlayers;
new vipsNumber;

public plugin_init() 
{
	register_plugin("VIPSystem", "1.5", "ZETA [M|E|N]");
	
	register_dictionary("VIPSystem.txt");
	
	cvarPasswordField = register_cvar("vs_password_field", "_pw");
	
	forwardVipConnect = CreateMultiForward("VSVipConnect", ET_CONTINUE, FP_CELL);
	
	arrayset(vipFlags, 0, 33);
	
	maxPlayers = get_maxplayers();
	vipsNumber = 0;
	
	server_cmd("exec %s", GetPath("vips.cfg"));
	
	LoadVipList();
	
	register_srvcmd("vips_list", "ShowVipsList", ADMIN_ALL, "Show Vips List");
	register_clcmd("addvip", "AddVip", ADMIN_ALL, "Add Vip");
}

public plugin_precache()
{
	vipAuthArray = ArrayCreate(44, 1);
	vipPasswordArray = ArrayCreate(32, 1);
	vipAccountFlagsArray = ArrayCreate(1, 1);
	vipAccessFlagsArray = ArrayCreate(1, 1);
}

LoadVip(const auth[], const password[], const accessFlags[], const accountFlags[])
{
	ArrayPushString(vipAuthArray, auth);
	ArrayPushString(vipPasswordArray, password);
	ArrayPushCell(vipAccessFlagsArray, StrToBits(accessFlags));
	ArrayPushCell(vipAccountFlagsArray, StrToBits(accountFlags));
}

LoadVipList()
{
	new currentDate[9];
	get_time("%d.%m.%y", currentDate, charsmax(currentDate));
	
	new file = fopen(GetPath("vips.ini"), "r+");
	
	if (!file)
	{
		log_to_file("VIPSystem.txt", "LoadVipList(): %L", LANG_SERVER, "NO_VIPS_FILE");
		return;
	}
	
	new text[121], auth[44], password[32], accessFlags[27], accountFlags[3], expiryDate[9], pos;
	while (!feof(file))
	{
		pos = ftell(file);
		fgets(file, text, charsmax(text));
		
		trim(text);
		
		if ((text[0] == ';') || !strlen(text)) 
		{ 
			continue; 
		}
		
		if (parse(text, auth, charsmax(auth), 
			password, charsmax(password), 
			accessFlags, charsmax(accessFlags), 
			accountFlags, charsmax(accountFlags), 
			expiryDate, charsmax(expiryDate)) != 5)
		{
			log_to_file("VIPSystem.txt", "LoadVipList(): %L", LANG_SERVER, "INVALID_FORMAT", text);
			continue;
		}
		
		if (equal(currentDate, expiryDate))
		{
			fseek(file, pos, SEEK_SET); 
			fprintf(file, ";%s", text);
			fseek(file, 0, SEEK_CUR);
			continue;
		}
			
		LoadVip(auth, password, accessFlags, accountFlags);
		++vipsNumber;
	}
		
	fclose(file);
	
	switch (vipsNumber)
	{
		case 0: server_print("[VIPSystem] %L", LANG_SERVER, "NO_VIPS");
		case 1: server_print("[VIPSystem] %L", LANG_SERVER, "LOADED_VIP");
		default: server_print("[VIPSystem] %L", LANG_SERVER, "LOADED_VIPS", vipsNumber);
	}
}

RemoveAccess(const id)
{
	vipFlags[id] = 0;
}

GetAccess(const id)
{
	new userName[32], passwordField[32], userPassword[32], userAuth[32], userIp[44];
	get_user_info(id, "name", userName, charsmax(userName));
	get_pcvar_string(cvarPasswordField, passwordField, charsmax(passwordField));
	get_user_info(id, passwordField, userPassword, charsmax(userPassword));
	get_user_authid(id, userAuth, charsmax(userAuth));
	get_user_ip(id, userIp, charsmax(userIp), 1);
	
	RemoveAccess(id);
	
	new auth[44], password[32], accessFlags, accountFlags;
	for (new i = 0; i < vipsNumber; i++)
	{
		ArrayGetString(vipAuthArray, i, auth, charsmax(auth));
		ArrayGetString(vipPasswordArray, i, password, charsmax(password));
		accessFlags = ArrayGetCell(vipAccessFlagsArray, i);
		accountFlags = ArrayGetCell(vipAccountFlagsArray, i);
		
		if (((accountFlags & VIP_STEAM) && equal(auth, userAuth)) || 
			((accountFlags & VIP_IP) && equal(auth, userIp)))
		{
			vipFlags[id] = accessFlags;
			break;
		}
		else if ((accountFlags & VIP_NAME) && equal(auth, userName))
		{
			if (equal(password, userPassword))
			{
				vipFlags[id] = accessFlags;
			}
			else
			{
				server_cmd("kick #%d ^"%L^"", get_user_userid(id), id, "NO_ENTRY");
			}
			
			break;
		}
	}
}

ConnectGetAccess(const id)
{
	GetAccess(id);
	
	if (vipFlags[id])
	{
		new result;
		ExecuteForward(forwardVipConnect, result, id);
	}
}

// Events

public client_putinserver(id)
{
	ConnectGetAccess(id);
}

public client_disconnect(id)
{
	RemoveAccess(id);
}

public client_infochanged(id)
{
	new newname[32], oldname[32];
	get_user_name(id, oldname, charsmax(oldname));
	get_user_info(id, "name", newname, charsmax(newname));
	
	if (!equal(newname, oldname))
	{
		GetAccess(id);
	}
}

// Natives

public plugin_natives()
{	
	register_native("VSGetUserVip", "NativeGetUserVip", 1);
	register_native("VSGetVipFlag", "NativeGetVipFlag", 1);
	register_native("VSGetVipFlags", "NativeGetVipFlags", 1);
	register_native("VSAddVipFlags", "NativeAddVipFlags", 1);
}

public NativeGetUserVip(id)
{
	if (!IsUser(id))
	{
		log_to_file("VIPSystem.txt", "NativeGetUserVip(id): %L", LANG_SERVER, "OUT_OF_RANGE", id);
		return false;
	}
	
	return (vipFlags[id] != 0);
}

public NativeGetVipFlag(id, flag)
{
	if (!IsUser(id))
	{
		log_to_file("VIPSystem.txt", "NativeGetVipFlag(id, flag): %L", LANG_SERVER, "OUT_OF_RANGE", id);
		return false;
	}
	
	if (!vipFlags[id])
	{
		return false;
	}
	
	if (flag && !(vipFlags[id] & flag))
	{
		return false;
	}
	
	return true;
}

public NativeGetVipFlags(id)
{
	if (!IsUser(id))
	{
		log_to_file("VIPSystem.txt", "NativeGetVipFlags(id): %L", LANG_SERVER, "OUT_OF_RANGE", id);
		return 0;
	}
	
	return vipFlags[id];
}

public NativeAddVipFlags(id, flags)
{
	if (!IsUser(id))
	{
		log_to_file("VIPSystem.txt", "NativeGetVipFlag(id, flag): %L", LANG_SERVER, "OUT_OF_RANGE", id);
		return;
	}
	
	vipFlags[id] |= flags;
}

// Commands

public ShowVipsList(id)
{
	server_print("%L", LANG_SERVER, "VIPS_LIST");
	
	if (!vipsNumber)
	{
		server_print("%L", LANG_SERVER, "NO_VIPS");
		return PLUGIN_HANDLED;
	}
	
	new auth[44], password[32], accessFlags[26], accountFlags[3], accessFlagsBin, accountFlagsBin;
	for(new i = 0; i < vipsNumber; i++)
	{
		ArrayGetString(vipAuthArray, i, auth, charsmax(auth));
		ArrayGetString(vipPasswordArray, i, password, charsmax(password));
		accessFlagsBin = ArrayGetCell(vipAccessFlagsArray, i);
		accountFlagsBin = ArrayGetCell(vipAccountFlagsArray, i);
		
		BitsToStr(accessFlagsBin, accessFlags);
		
		BitsToStr(accountFlagsBin, accountFlags);
		
		server_print("^"%s^" ^"%s^" ^"%s^" ^"%s^"", auth, password, accessFlags, accountFlags);
	}
	
	return PLUGIN_HANDLED;
}

public AddVip(id)
{
	if (!(get_user_flags(id) & ADMIN_CVAR))
	{
		client_print(id, print_console, "%L", id, "NO_ACC_COM");
		return PLUGIN_HANDLED;
	}
	
	new text[121], auth[44], password[32], accessFlags[26], accountFlags[3], expiryDate[9];
	read_args(text, charsmax(text));

	if (parse(text, auth, charsmax(auth), 
		password, charsmax(password), 
		accessFlags, charsmax(accessFlags), 
		accountFlags, charsmax(accountFlags), 
		expiryDate, charsmax(expiryDate)) != 5)
	{
		client_print(id, print_console, "%L", LANG_SERVER, "FORMAT_ADD");
		return PLUGIN_HANDLED;
	}
	
	new file = fopen(GetPath("vips.ini"), "a");
	
	if (file)
	{
		fseek(file, 0, SEEK_END);
		fprintf(file, "^n^"%s^" ^"%s^" ^"%s^" ^"%s^" ^"%s^"", 
			auth, password, accessFlags, accountFlags, expiryDate);
		fclose(file);
	}
			
	LoadVip(auth, password, accessFlags, accountFlags);
	client_print(id, print_console, "[VIPSystem] %L", LANG_SERVER, "VIP_ADDED");
	return PLUGIN_HANDLED;
}

// Other

GetPath(const fileName[])
{
	new path[64];
	get_localinfo("amxx_configsdir", path, charsmax(path));
	
	format(path, charsmax(path), "%s/%s", path, fileName);
	return path;
}

StrToBits(const str[])
{
	new bin = 0;
	new len = strlen(str);
	
	for (new i = 0; i < len; ++i)
	{
		bin |= (1<<(str[i] - 'a'));
	}
	
	return bin;
}

BitsToStr(const bits, str[])
{
	new ch[2];
	
	for (new i = 0, len = 0; i < 27; ++i)
	{
		if (bits & (1<<i))
		{
			format(ch, charsmax(ch), "%c", (i + 'a'));
			strcat(str, ch, ++len);
		}
	}
}

IsUser(const id)
{
	return (1 <= id <= maxPlayers);
}

public plugin_end()
{
	ArrayDestroy(vipAuthArray);
	ArrayDestroy(vipPasswordArray);
	ArrayDestroy(vipAccountFlagsArray);
	ArrayDestroy(vipAccessFlagsArray);
}
